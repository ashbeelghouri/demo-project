import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiWorkerService } from '../api-worker.service';
import { Location } from '@angular/common';
@Component({
  selector: 'app-photos',
  templateUrl: './photos.component.html',
  styleUrls: ['./photos.component.css']
})
export class PhotosComponent implements OnInit {
  photo = {};
  loaded = false;
  photoid:any = false;
  constructor(private router: ActivatedRoute, private api: ApiWorkerService, private location: Location) { }

  viewerOpen = false;

  ngOnInit(): void {
    this.photoid = this.router.snapshot.params.photoid;
    this.getPhoto();
  }
  goBack() {
    // window.history.back();
    this.location.back();
    console.log( 'goBack()...' );
  }
  getPhoto = () => {
    this.api.getPhotos(false, this.photoid)
    .subscribe((data:any)=>{
      console.log(data);
      this.photo = data[0];
      this.loaded = true;
    });
  };

}
