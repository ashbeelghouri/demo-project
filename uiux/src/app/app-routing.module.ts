import { NgModule, Component } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { UsersListComponent } from './users-list/users-list.component';
import { AlbulmsComponent } from './albulms/albulms.component';
import { PhotosComponent } from './photos/photos.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AlbulmDetailsComponent } from './albulm-details/albulm-details.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: '/users',
        pathMatch: 'full'
    },
    {
        path: 'users',
        component: UsersListComponent
    },
    {
        path: 'albulms/:userid',
        component: AlbulmsComponent
    },
    {
        path: 'albulm-details/:albulmid',
        component: AlbulmDetailsComponent
    },
    {
        path: 'photos/:photoid',
        component: PhotosComponent
    },
    {
        path: '**',
        component: PageNotFoundComponent
    }

];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }

export const routingComponents = [
    UsersListComponent,
    AlbulmsComponent,
    PhotosComponent
];