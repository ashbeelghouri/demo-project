import { Component, OnInit } from '@angular/core';
import { ApiWorkerService } from '../api-worker.service';

@Component({
  selector: 'app-users-card',
  templateUrl: './users-card.component.html',
  styleUrls: ['./users-card.component.css']
})

export class UsersCardComponent implements OnInit {

  constructor(private api: ApiWorkerService) { }
  usersList:any = [];
  loaded = false;
  ngOnInit(): void {
    this.getUsersList();
  }
  getUsersList = () => {
    this.api.getUsersList()
    .subscribe((data:any)=>{
      console.log(data);
      this.usersList = data;
      this.loaded = true;
    });
  };
}
