import { TestBed } from '@angular/core/testing';

import { ApiWorkerService } from './api-worker.service';

describe('ApiWorkerService', () => {
  let service: ApiWorkerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApiWorkerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
