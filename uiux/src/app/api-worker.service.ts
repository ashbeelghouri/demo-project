import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ApiWorkerService {
  urls = { 
    users: "https://jsonplaceholder.typicode.com/users",
    albulms: "https://jsonplaceholder.typicode.com/albums",
    photos: "https://jsonplaceholder.typicode.com/photos"
  };
  constructor(private http: HttpClient) { }

  getUsersList = (userid:any=false) => {
    let url = userid ? this.urls.users + "?id=" + userid :  this.urls.users;
    return this.http.get(url);
  };

  getAlbulms = (userid:any=false, albulmId:any=false) => {
    let params = [];
    
    if(albulmId){
      params.push("id=" + albulmId);
    }
    if(userid){
      params.push("userId="+userid);
    }
    let url = params.length > 0 ? this.urls.albulms + "?" + params.join("&") :  this.urls.albulms;
    return this.http.get(url);
  }

  getPhotos = (albulmId:any=false, photoId:any=false) => {
    let params = [];
    
    if(photoId){
      params.push("id=" + photoId);
    }
    if(albulmId){
      params.push("albulmId="+albulmId);
    }
    let url = params.length > 0 ? this.urls.photos + "?" + params.join("&") :  this.urls.photos;
    return this.http.get(url);
  }
}
