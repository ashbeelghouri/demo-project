import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiWorkerService } from '../api-worker.service';
import { Location } from '@angular/common';
@Component({
  selector: 'app-albulms',
  templateUrl: './albulms.component.html',
  styleUrls: ['./albulms.component.css']
})
export class AlbulmsComponent implements OnInit {
  albulmList:any = [];
  loaded = false;
  constructor(private router: ActivatedRoute, private api: ApiWorkerService, private location: Location) { }
  userid:any = false;
  ngOnInit(): void {
    this.userid = this.router.snapshot.params.userid;
    this.getUserAlbulms();
  }
  goBack() {
    // window.history.back();
    this.location.back();
    console.log( 'goBack()...' );
  }
  getUserAlbulms = () => {
    this.api.getAlbulms(this.userid)
    .subscribe((data:any)=>{
      console.log(data);
      this.albulmList = data;
      this.getPhotos();
    });
  };

  getPhotos = () => {
    if(this.albulmList.length > 0){
      for(let i = 0; i < this.albulmList.length; i++){
        let albulmid = this.albulmList[i].id;
        this.api.getPhotos(albulmid)
        .subscribe((data:any) => {
          console.log(data);
          let photothumbs = [];
          for(let j = 0; (j < data.length && j < 4); j++){
            photothumbs.push(data[j].thumbnailUrl);
          }
          this.albulmList[i].photos = photothumbs;
          this.loaded = true;
        });
      }
    }
  };

}
