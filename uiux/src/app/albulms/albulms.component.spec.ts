import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlbulmsComponent } from './albulms.component';

describe('AlbulmsComponent', () => {
  let component: AlbulmsComponent;
  let fixture: ComponentFixture<AlbulmsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlbulmsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlbulmsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
