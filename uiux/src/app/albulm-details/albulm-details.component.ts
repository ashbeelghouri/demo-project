import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiWorkerService } from '../api-worker.service';
import { Location } from '@angular/common';
@Component({
  selector: 'app-albulm-details',
  templateUrl: './albulm-details.component.html',
  styleUrls: ['./albulm-details.component.css']
})
export class AlbulmDetailsComponent implements OnInit {
  photosList:any = [];
  loaded = false;
  constructor(private router: ActivatedRoute, private api: ApiWorkerService, private location: Location) { }
  albulmid:any = false;
  ngOnInit(): void {
    this.albulmid = this.router.snapshot.params.albulmid;
    this.getAlbulmPhotos()
  }
  goBack() {
    // window.history.back();
    this.location.back();
    console.log( 'goBack()...' );
  }
  getAlbulmPhotos = () => {
    this.api.getPhotos(this.albulmid)
    .subscribe((data:any)=>{
      console.log(data);
      this.photosList = data;
      this.loaded = true;
    });
  };

}
