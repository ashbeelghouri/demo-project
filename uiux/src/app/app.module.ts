import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { HeaderComponent } from './header/header.component';
import { ContentsComponent } from './contents/contents.component';
import { UsersCardComponent } from './users-card/users-card.component';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule, routingComponents } from './app-routing.module';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AlbulmDetailsComponent } from './albulm-details/albulm-details.component';
import { LoaderComponent } from './loader/loader.component';
import { ImageViewerComponent } from './image-viewer/image-viewer.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ContentsComponent,
    UsersCardComponent,
    routingComponents,
    PageNotFoundComponent,
    AlbulmDetailsComponent,
    LoaderComponent,
    ImageViewerComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
